extends Node

signal record_started()
signal record_finished()



var effect: AudioEffectRecord
var recording: AudioStreamWAV



func _ready() -> void:
	var bus_index = AudioServer.get_bus_index("rec")
	effect = AudioServer.get_bus_effect(bus_index, 0)



func start_recording() -> void:
	if not effect.is_recording_active():
		recording = null
		effect.set_recording_active(true)
		record_started.emit()

func finish_recording() -> AudioStream:
	if effect.is_recording_active():
		recording = effect.get_recording()
		effect.set_recording_active(false)
		record_finished.emit()
		return recording
	
	return null

func is_recording() -> bool:
	return effect.is_recording_active()

