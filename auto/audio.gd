extends Node


signal clip_finished()
signal record_finished()



func set_clip_volume(db: float) -> void:
	$clip.volume_db = db

func set_record_volume(db: float) -> void:
	$record.volume_db = db

func set_clip(s: AudioStream) -> void:
	$clip.stream = s

func set_record(s: AudioStream) -> void:
	$record.stream = s



func play_clip() -> void:
	$clip.play()

func play_record() -> void:
	$record.play()



func stop() -> void:
	$clip.stop()
	$record.stop()



func _on_clip_finished() -> void:
	clip_finished.emit()

func _on_record_finished() -> void:
	record_finished.emit()
