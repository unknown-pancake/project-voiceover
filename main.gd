extends PanelContainer


var play_and_record := false



func _ready() -> void:
	populate_clip_tree()
	Audio.clip_finished.connect(_on_audio_clip_finished)



func populate_clip_tree() -> void:
	var voices = Data.get_voice_and_clip_list()
	
	var root: TreeItem = $%clips.create_item()
	
	for k in voices.keys():
		var v = root.create_child()
		v.set_text(0, k)
		
		for c in voices[k]:
			var i = v.create_child()
			i.set_text(0, c.split(".", false, 1)[0])
			i.set_meta("clip_path", Data.root.path_join(k).path_join(c))



func start_recording() -> void:
	if Recorder.is_recording():
		stop_recording()
	
	Recorder.start_recording()
	$%record.text = "Stop Recording"

func stop_recording() -> void:
	if Recorder.is_recording():
		Audio.set_record(Recorder.finish_recording())
		$%record.text = "Record"



func _on_audio_clip_finished() -> void:
	if play_and_record:
		stop_recording()
		play_and_record = false



func _on_open_folder_pressed() -> void:
	Data.open_folder()

func _on_clips_item_selected() -> void:
	var item = $%clips.get_selected()
	
	if item.has_meta("clip_path"):
		var clip = Data.load_clip(item.get_meta("clip_path"))
		Audio.set_clip(clip)

func _on_play_clip_pressed() -> void:
	Audio.play_clip()

func _on_stop_pressed() -> void:
	Audio.stop()
	stop_recording()

func _on_record_pressed() -> void:
	if Recorder.is_recording():
		stop_recording()
	
	else:
		start_recording()

func _on_play_record_pressed() -> void:
	Audio.play_record()

func _on_play_and_record_pressed() -> void:
	Audio.play_clip()
	start_recording()
	play_and_record = true

func _on_play_with_voiceover_pressed() -> void:
	Audio.play_clip()
	Audio.play_record()

func _on_clip_volume_range_value_changed(value: float) -> void:
	Audio.set_clip_volume(value)
	$%clip_volume_box.value = value

func _on_clip_volume_box_value_changed(value: float) -> void:
	Audio.set_clip_volume(value)
	$%clip_volume_range.value = value

func _on_record_volume_range_value_changed(value: float) -> void:
	Audio.set_record_volume(value)
	$%record_volume_box.value = value

func _on_record_volume_box_value_changed(value: float) -> void:
	Audio.set_record_volume(value)
	$%record_volume_range.value = value
