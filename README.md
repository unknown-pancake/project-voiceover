
Software made for voice training (like practicing a new accent or transvoice practice).

It allows you to play audio clips, record over them, and listen back quickly.

# Adding clips

1. Start PVO
2. Click on "Open clip folder"
3. Create a new folder for the voice/accent
4. Put clips (.wav, .mp3, .ogg supported) in the folder
5. Restart PVO

The clips are now available in PVO.

*Note* : Step 3 is mandatory. Clips directly put in the clip folder wont be detected unless in a subdirectory.

# Usage

After selecting a clip on the left panel :

- `Play` will simply play the current clip.
- `Play & record` will play the current clip and will start recording. The recording will stop at the end of the current audio clip.
- `Play with voiceover` will play the last recording and the current clip at the same time.
- `Stop` will stop the current clip (if playing), the last recording (if playing), and the current recording (if recording).
- `Clip volume` allows you to change the volume of the clip playback.
- `Voiceover volume` allows you to change the volume of the last recording playback.
- `Record` will start recording. After pressing it, pressing it again will stop recording.
- `Play voiceover` will play the last recording.

# Bugs

## The recording is corrupted

Godot 4 bug, restart the software.

## The recording doesn't change anymore, even after pressing Stop or Record

Bug from PVO or Godot 4, i dont know. Restart the software.