extends Node


var root: String = "user://voices"


func _ready() -> void:
	ensure_folder_structure()


func ensure_folder_structure() -> void:
	if not DirAccess.dir_exists_absolute(root):
		DirAccess.make_dir_recursive_absolute(root)

func open_folder() -> void:
	OS.shell_open(ProjectSettings.globalize_path(root))



func get_voice_list() -> PackedStringArray:
	var dir = DirAccess.open(root)
	return dir.get_directories()

func get_clip_list(voice: String) -> PackedStringArray:
	var dir = DirAccess.open(root.path_join(voice))
	return dir.get_files()

func get_voice_and_clip_list() -> Dictionary:
	var voices = get_voice_list()
	var res = {}
	
	for voice in voices:
		res[voice] = get_clip_list(voice)
	
	return res



func load_clip(path: String) -> AudioStream:
	if path.ends_with(".wav"):
		return load_clip_wav(path)
	elif path.ends_with(".mp3"):
		return load_clip_mp3(path)
	elif path.ends_with(".ogg"):
		return load_clip_ogg(path)
	else:
		printerr("Unsupported format '" + path + "'.")
		return null

func load_clip_wav(path: String) -> AudioStreamWAV:
	var buf = _load_buffer(path)
	var sound = AudioStreamWAV.new()
	sound.data = buf
	return sound

func load_clip_mp3(path: String) -> AudioStreamMP3:
	var buf = _load_buffer(path)
	var sound = AudioStreamMP3.new()
	sound.data = buf
	return sound

func load_clip_ogg(path: String) -> AudioStreamOggVorbis:
	var buf = _load_buffer(path)
	var sound = AudioStreamOggVorbis.new()
	sound.data = buf
	return sound



func _load_buffer(path: String) -> PackedByteArray:
	var file = FileAccess.open(path, FileAccess.READ)
	return file.get_buffer(file.get_length())
